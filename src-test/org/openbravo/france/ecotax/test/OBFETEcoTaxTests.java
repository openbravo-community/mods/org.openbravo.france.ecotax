/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2020-2021 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.france.ecotax.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.comparesEqualTo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.weld.WeldUtils;
import org.openbravo.base.weld.test.WeldBaseTest;
import org.openbravo.common.actionhandler.createlinesfromprocess.CreateInvoiceLinesFromProcess;
import org.openbravo.dal.service.OBDal;
import org.openbravo.france.ecotax.PhiecoTaxcategory;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;

public class OBFETEcoTaxTests extends WeldBaseTest {
  private static final Logger log = LogManager.getLogger();

  private PhiecoTaxcategory ecotaxcategoryDEA;
  private PhiecoTaxcategory ecotaxcategoryDEEE;
  private Product productDEA;
  private Product productDEEE;
  private Product productDEADEEE;

  /**
   * <ul>
   * <li>Create Eco Tax Categories</li>
   *
   * <li>Create a Eco Tax Category with Type DEA and Tax Amount 0.50</li>
   * <li>Create a Eco Tax Category with Type DEEE and Tax Amount 1.00</li>
   * 
   * <li>Create Products by assigning Eco Taxes</li>
   * 
   * <li>Create a Product with DEA Tax: 0.50 and DEA Qty: 1.00, List Price = Standard Price as 10.00
   * in Tarifa de ventas Price List</li>
   * <li>Create a Product with DEEE Tax: 1.00 and DEEE Qty: 1.00), List Price = Standard Price as
   * 50.00 in Tarifa de ventas Price List</li>
   * <li>Create a Product with both DEA Tax 0.50, DEA Qty 1.00 and DEEE Tax: 1.00, DEEE Qty 1.00,
   * List Price = Standard Price as 100.00 in Tarifa de ventas Price List</li>
   * </ul>
   */
  @Before
  public void setUpEcoTaxTest() {

    OBFETUtility.setTestContextSpain();

    String key = System.currentTimeMillis() + "";

    // Create DEA Eco Tax Category
    ecotaxcategoryDEA = OBFETUtility.createNewEcoTaxCategory(OBFETConstants.ECO_TAX_DEA + key,
        OBFETConstants.ECO_TAX_DEA + key, OBFETConstants.ECO_TAX_TYPE_DEA, new BigDecimal("0.50"));

    // Create DEEE Eco Tax Category
    ecotaxcategoryDEEE = OBFETUtility.createNewEcoTaxCategory(OBFETConstants.ECO_TAX_DEEE + key,
        OBFETConstants.ECO_TAX_DEEE + key, OBFETConstants.ECO_TAX_TYPE_DEEE, BigDecimal.ONE);

    // Create Product with only DEA Type Eco Tax defined
    productDEA = OBFETUtility.createNewProductWithParams(key + "_1", key + "_1", ecotaxcategoryDEA,
        BigDecimal.ONE, null, null, BigDecimal.TEN, BigDecimal.TEN);

    // Create Product with only DEEE Type Eco Tax defined
    productDEEE = OBFETUtility.createNewProductWithParams(key + "_2", key + "_2", null, null,
        ecotaxcategoryDEEE, BigDecimal.ONE, new BigDecimal("50.00"), new BigDecimal("50.00"));

    // Create Product with both DEA and DEEE Eco Tax Types defined
    productDEADEEE = OBFETUtility.createNewProductWithParams(key + "_3", key + "_3",
        ecotaxcategoryDEA, BigDecimal.ONE, ecotaxcategoryDEEE, BigDecimal.ONE,
        new BigDecimal("100.00"), new BigDecimal("100.00"));

  }

  /**
   * <ul>
   * <li>Create Sales Order and Lines</li>
   * 
   * <li>Header: Org:F&B Espana, Business Partner: Hoteles Buenas Noches, S.A</li>
   * <li>Line1: Product:Ist Product with DEA Eco Tax Category, Quantity:10</li>
   * <li>Verify DeaNet Amount: 5.00, DeeeNet Amt: 0.00</li>
   * <li>Line2: Product:IInd Product with DEA Eco Tax Category, Quantity:10</li>
   * <li>Verify DeaNet Amount: 0.00, DeeeNet Amt: 10.00</li>
   * <li>Line3: Product:IIIrd Product with DEA & DEEE Eco Tax Category, Quantity:10</li>
   * <li>Verify DeaNet Amount: 5.00, DeeeNet Amt: 10.00</li>
   * <li>Delete OrderLines and Order record created for test</li>
   * </ul>
   */

  @Test
  public void testEcoTaxSalesOrderLineTest() {
    try {

      // Create Sales Order and Lines
      Order order = OBFETUtility.createNewSalesOrderWithDefaultParameters();

      // OrderLine with Product having only DEA Tax
      OrderLine orderlineDEA = OBFETUtility.createNewSalesOrderLineWithDefaultParameters(order,
          productDEA);
      OBFETUtility.reloadAndRefreshOrderLine(orderlineDEA);
      assertThat("DeaNet Amount does not match", new BigDecimal("5.00"),
          comparesEqualTo(orderlineDEA.getPhiecoDeanet()));
      assertThat("DeeeNetAmount does not match", BigDecimal.ZERO,
          comparesEqualTo(orderlineDEA.getPhiecoDeeenet()));

      // OrderLine with Product having only DEEE Tax
      OrderLine orderlineDEEE = OBFETUtility.createNewSalesOrderLineWithDefaultParameters(order,
          productDEEE);
      OBFETUtility.reloadAndRefreshOrderLine(orderlineDEEE);
      assertThat("DeaNet Amount does not match", BigDecimal.ZERO,
          comparesEqualTo(orderlineDEEE.getPhiecoDeanet()));
      assertThat("DeeeNet Amount does not match", BigDecimal.TEN,
          comparesEqualTo(orderlineDEEE.getPhiecoDeeenet()));

      // OrderLine with Product having both DEA and DEEE Tax
      OrderLine orderlineDEADEEE = OBFETUtility.createNewSalesOrderLineWithDefaultParameters(order,
          productDEADEEE);
      OBFETUtility.reloadAndRefreshOrderLine(orderlineDEADEEE);
      assertThat("DeeeNet Amount does not match", BigDecimal.TEN,
          comparesEqualTo(orderlineDEADEEE.getPhiecoDeeenet()));
      assertThat("DeaNet Amount does not match", new BigDecimal("5.00"),
          comparesEqualTo(orderlineDEADEEE.getPhiecoDeanet()));

      // Delete Order and its OrderLines created for test
      delete(orderlineDEADEEE);
      delete(orderlineDEEE);
      delete(orderlineDEA);
      delete(order);

    } catch (Exception e) {
      log.error("Error in testEcoTaxSalesOrderLineTest", e);
    }
  }

  /**
   * <ul>
   * <li>Create Sales Invoice and Lines</li>
   * 
   * <li>Header: Org:F&B Espana, Business Partner: Hoteles Buenas Noches, S.A</li>
   * <li>Line1: Product:Ist Product with DEA Eco Tax Category, Quantity:10</li>
   * <li>Verify DeaNet Amount: 5.00, DeeeNet Amt: 0.00</li>
   * <li>Line2: Product:IInd Product with DEA Eco Tax Category, Quantity:10</li>
   * <li>Verify DeaNet Amount: 0.00, DeeeNet Amt: 10.00</li>
   * <li>Line3: Product:IIIrd Product with DEA & DEEE Eco Tax Category, Quantity:10</li>
   * <li>Verify DeaNet Amount: 5.00, DeeeNet Amt: 10.00</li>
   * <li>Delete InvoiceLines and Invoice record created for test</li>
   * </ul>
   */
  @Test
  public void testEcoTaxSalesInvoiceLineTest() {
    try {

      // Create Sales Invoice and Lines
      Invoice invoice = OBFETUtility.createNewSalesInvoiceWithDefaultParameters();

      // InvoiceLine with Product having only DEA Tax
      InvoiceLine invoicelineDEA = OBFETUtility
          .createNewSalesInvoiceLineWithDefaultParameters(invoice, productDEA);
      OBFETUtility.reloadAndRefreshInvoiceLine(invoicelineDEA);
      assertThat("DeaNet Amount does not match", new BigDecimal("5.00"),
          comparesEqualTo(invoicelineDEA.getPhiecoDeanet()));
      assertThat("DeeeNet Amount does not match", BigDecimal.ZERO,
          comparesEqualTo(invoicelineDEA.getPhiecoDeeenet()));

      // InvoiceLine with Product having only DEEE Tax
      InvoiceLine invoicelineDEEE = OBFETUtility
          .createNewSalesInvoiceLineWithDefaultParameters(invoice, productDEEE);
      OBFETUtility.reloadAndRefreshInvoiceLine(invoicelineDEEE);
      assertThat("DeaNet Amount does not match", BigDecimal.ZERO,
          comparesEqualTo(invoicelineDEEE.getPhiecoDeanet()));
      assertThat("DeeeNet Amount does not match", BigDecimal.TEN,
          comparesEqualTo(invoicelineDEEE.getPhiecoDeeenet()));

      // InvoiceLine with Product having both DEA and DEEE Tax
      InvoiceLine invoicelineDEADEEE = OBFETUtility
          .createNewSalesInvoiceLineWithDefaultParameters(invoice, productDEADEEE);
      OBFETUtility.reloadAndRefreshInvoiceLine(invoicelineDEADEEE);
      assertThat("DeeeNet Amount does not match", BigDecimal.TEN,
          comparesEqualTo(invoicelineDEADEEE.getPhiecoDeeenet()));
      assertThat("DeaNet Amount does not match", new BigDecimal("5.00"),
          comparesEqualTo(invoicelineDEADEEE.getPhiecoDeanet()));

      // Delete Invoice & its InvoiceLines created for test
      delete(invoicelineDEADEEE);
      delete(invoicelineDEEE);
      delete(invoicelineDEA);
      delete(invoice);

    } catch (Exception e) {
      log.error("Error in testEcoTaxSalesInvoiceLineTest", e);
    }
  }

  /**
   * <li>Create Sales Invoice Lines from Order</li>
   * 
   * <li>Sales Order Header:- Org:F&B Espana, Business Partner: Hoteles Buenas Noches, S.A</li>
   * <li>Line1: Product with DEA Eco Tax Category, Quantity:10</li>
   * <li>Line2: Product with DEEE Eco Tax Category, Quantity:10</li>
   * <li>Line3: Product with both DEA & DEEE Eco Tax Category, Quantity:10</li>
   * <li>Book the Order</li>
   * <li>Sales Invoice Header:- Org:F&B Espana, Business Partner: Hoteles Buenas Noches, S.A</li>
   * <li>Using create lines from order process, create invoice lines</li> *
   * <li>Verify Invoice Line:Product with Dea Tax as DeaNet Amount: 5.00, DeeeNet Amt: 0.00</li>
   * <li>Verify Invoice Line:Product with Deee Tax as DeaNet Amount: 0.00, DeeeNet Amt: 10.00</li>
   * <li>Verify Invoice Line:Product with both Dea & Deee Tax as DeaNet Amount: 5.00, DeeeNet Amt:
   * 10.00</li>
   * <li>Delete InvoiceLines and Invoice record created for test</li>
   * <li>Reactivate Order, delete orderLines and Order record created for test</li>
   * </ul>
   */
  @Test
  public void testEcoTaxCreateSalesInvoiceLinesFromOrderTest() {
    try {

      // Create Sales Order and Lines
      Order order = OBFETUtility.createNewSalesOrderWithDefaultParameters();

      // Create three OrderLines, one each with Product having only DEA Tax, Only DEEE Tax and both
      // DEA & DEEE Tax
      OBFETUtility.createNewSalesOrderLineWithDefaultParameters(order, productDEA);
      OBFETUtility.createNewSalesOrderLineWithDefaultParameters(order, productDEEE);
      OBFETUtility.createNewSalesOrderLineWithDefaultParameters(order, productDEADEEE);
      OBFETUtility.processOrder(order);
      OBDal.getInstance().refresh(order);

      // Create Sales Invoice Header
      Invoice invoice = OBFETUtility.createNewSalesInvoiceWithDefaultParameters();
      JSONArray selectedLines = OBFETUtility.createSelectedLinesFromOrder(order);

      // CreateLinesFromProcess is instantiated using Weld so it can use Dependency Injection to
      // create Sales Invoice Lines From Order
      CreateInvoiceLinesFromProcess createLinesFromProcess = WeldUtils
          .getInstanceFromStaticBeanManager(CreateInvoiceLinesFromProcess.class);
      createLinesFromProcess.createInvoiceLinesFromDocumentLines(selectedLines, invoice,
          OrderLine.class);
      // Refresh Invoice
      OBDal.getInstance().refresh(invoice);

      // Check each line DEA and DEEE Tax Amount
      List<InvoiceLine> invoiceLineListToRemove = new ArrayList<InvoiceLine>();
      List<OrderLine> orderLineListToRemove = new ArrayList<OrderLine>();
      for (InvoiceLine invoiceLine : invoice.getInvoiceLineList()) {

        if (StringUtils.equals(invoiceLine.getProduct().getId(), productDEA.getId())) {
          assertThat("DeaNet Amount does not match", new BigDecimal("5.00"),
              comparesEqualTo(invoiceLine.getPhiecoDeanet()));
          assertThat("DeeeNet Amount does not match", BigDecimal.ZERO,
              comparesEqualTo(invoiceLine.getPhiecoDeeenet()));
        }

        if (StringUtils.equals(invoiceLine.getProduct().getId(), productDEEE.getId())) {
          assertThat("DeaNet Amount does not match", BigDecimal.ZERO,
              comparesEqualTo(invoiceLine.getPhiecoDeanet()));
          assertThat("DeeeNet Amount does not match", BigDecimal.TEN,
              comparesEqualTo(invoiceLine.getPhiecoDeeenet()));
        }

        if (StringUtils.equals(invoiceLine.getProduct().getId(), productDEADEEE.getId())) {
          assertThat("DeeeNet Amount does not match", BigDecimal.TEN,
              comparesEqualTo(invoiceLine.getPhiecoDeeenet()));
          assertThat("DeaNet Amount does not match", new BigDecimal("5.00"),
              comparesEqualTo(invoiceLine.getPhiecoDeanet()));
        }
        invoiceLineListToRemove.add(invoiceLine);
        orderLineListToRemove.add(invoiceLine.getSalesOrderLine());
      }
      // Delete Invoice & its InvoiceLines created for test
      for (InvoiceLine lineToremove : invoiceLineListToRemove) {
        invoice.getInvoiceLineList().remove(lineToremove);
        OBDal.getInstance().remove(lineToremove);
      }
      OBDal.getInstance().flush();
      OBDal.getInstance().refresh(invoice);
      delete(invoice);

      // Reactivate and Refresh Order
      OBFETUtility.reactivateOrder(order);
      OBDal.getInstance().refresh(order);

      // Delete Order and its OrderLines created for test
      for (OrderLine lineToremove : orderLineListToRemove) {
        order.getOrderLineList().remove(lineToremove);
        OBDal.getInstance().remove(lineToremove);
      }
      OBDal.getInstance().flush();
      OBDal.getInstance().refresh(order);
      delete(order);

    } catch (Exception e) {
      log.error("Error in testEcoTaxCreateSalesInvoiceLinesFromOrderTest", e);
    }
  }

  /**
   * <ul>
   * <li>Delete product created for the tests</li>
   * <li>Delete Eco Tax Categories created for test</li>
   * </ul>
   */
  @After
  public void cleanUpEcoTaxTest() {
    // Remove products created for test
    delete(productDEA);
    delete(productDEEE);
    delete(productDEADEEE);
    // Remove Eco Tax Categories created for test
    delete(ecotaxcategoryDEA);
    delete(ecotaxcategoryDEEE);
  }

  private void delete(BaseOBObject obj) {
    if (obj != null) {
      OBDal.getInstance().remove(obj);
      OBDal.getInstance().flush();
    }
  }
}
