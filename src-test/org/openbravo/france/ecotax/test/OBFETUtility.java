/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2020 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.france.ecotax.test;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.france.ecotax.PhiecoTaxcategory;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.ad.ui.Process;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
import org.openbravo.model.pricing.pricelist.ProductPrice;
import org.openbravo.service.db.CallProcess;

public class OBFETUtility {

  private static final Logger log = LogManager.getLogger();

  private OBFETUtility() {
    throw new IllegalStateException("FETUtility class");
  }

  /**
   * Set the default context for the Automated Tests with:
   * 
   * <ul>
   * <li>User: Openbravo</li>
   * <li>Role: F&amp;B International Group Administrator</li>
   * <li>Client: F&amp;B International Group</li>
   * <li>Organization: F&amp;B España</li>
   * </ul>
   */
  public static void setTestContextSpain() {
    OBContext.setOBContext(OBFETConstants.OPENBRAVO_USER_ID,
        OBFETConstants.FNB_INT_GROUP_ADMIN_ROLE_ID, OBFETConstants.FNB_GROUP_CLIENT_ID,
        OBFETConstants.FNB_ESPANA_ORG_ID);
  }

  public static PhiecoTaxcategory createNewEcoTaxCategory(String searchKey, String name,
      String phiecoType, BigDecimal taxAmount) {
    OBFETEcoTaxCategoryParameters ecoTaxCategoryParams = new OBFETEcoTaxCategoryParameters(
        searchKey, name, phiecoType, taxAmount);
    return createNewEcoTaxCategory(ecoTaxCategoryParams);
  }

  private static PhiecoTaxcategory createNewEcoTaxCategory(
      OBFETEcoTaxCategoryParameters ecoTaxCategoryParams) {
    PhiecoTaxcategory ecoTaxCategory = OBProvider.getInstance().get(PhiecoTaxcategory.class);
    ecoTaxCategory.setClient(ecoTaxCategory.getClient());
    ecoTaxCategory.setOrganization(ecoTaxCategoryParams.getOrganization());
    ecoTaxCategory.setSearchKey(ecoTaxCategoryParams.getSearchKey());
    ecoTaxCategory.setCommercialName(ecoTaxCategoryParams.getName());
    ecoTaxCategory.setPhiecoType(ecoTaxCategoryParams.getEcoTaxType());
    ecoTaxCategory.setTaxamt(ecoTaxCategoryParams.getTaxAmount());
    OBDal.getInstance().save(ecoTaxCategory);
    OBDal.getInstance().flush();
    return ecoTaxCategory;

  }

  public static Product createNewProductWithParams(String searchKey, String name,
      PhiecoTaxcategory phiecoDea, BigDecimal phiecoDeaQty, PhiecoTaxcategory phiecoDeee,
      BigDecimal phiecoDeeeQty, BigDecimal listPrice, BigDecimal standardPrice) {
    OBFETProductParameters productParams = new OBFETProductParameters(searchKey, name, phiecoDea,
        phiecoDeaQty, phiecoDeee, phiecoDeeeQty, listPrice, standardPrice);
    return createNewProduct(productParams);
  }

  private static Product createNewProduct(OBFETProductParameters productParams) {
    Product product = OBProvider.getInstance().get(Product.class);
    product.setClient(productParams.getClient());
    product.setOrganization(productParams.getOrganization());
    product.setSearchKey(productParams.getSearchKey());
    product.setName(productParams.getName());
    product.setProductCategory(productParams.getProductCategory());
    product.setProductType(productParams.getProductType());
    product.setTaxCategory(productParams.getTaxCategory());
    product.setUOM(productParams.getUom());
    product.setStocked(true);
    product.setSale(true);
    product.setPhiecoDea(productParams.getPhiecoDea());
    product.setPhiecoDeaQty(productParams.getPhiecoDeaQty());
    product.setPhiecoDeee(productParams.getPhiecoDeee());
    product.setPhiecoDeeeQty(productParams.getPhiecoDeeeQty());
    OBDal.getInstance().save(product);
    OBDal.getInstance().flush();
    // set Product Price
    setProductPrice(product, productParams.getListPrice(), productParams.getStandardPrice());
    return product;
  }

  private static void setProductPrice(Product product, BigDecimal listPrice,
      BigDecimal standardPrice) {
    ProductPrice productPrice = OBProvider.getInstance().get(ProductPrice.class);
    productPrice.setClient(product.getClient());
    productPrice.setOrganization(product.getOrganization());
    productPrice.setPriceListVersion(
        OBDal.getInstance().get(PriceListVersion.class, OBFETConstants.PRICELIST_VERSION_ID));
    productPrice.setProduct(product);
    productPrice.setListPrice(listPrice);
    productPrice.setStandardPrice(standardPrice);
    OBDal.getInstance().save(productPrice);
    OBDal.getInstance().flush();
  }

  /**
   * Creates and saves a New {@link Order} with the default Parameters for the automated Tests
   */
  public static Order createNewSalesOrderWithDefaultParameters() {
    OBFETOrderHeaderParameters orderHeaderParameters = new OBFETOrderHeaderParameters();
    return OBFETUtility.createNewOrder(orderHeaderParameters);
  }

  private static Order createNewOrder(OBFETOrderHeaderParameters parameters) {
    Order order = OBProvider.getInstance().get(Order.class);
    order.setOrganization(parameters.getOrganization());
    order.setClient(parameters.getClient());
    order.setDocumentType(parameters.getDocumentType());
    order.setTransactionDocument(parameters.getDocumentType());
    order.setDocumentNo(FIN_Utility.getDocumentNo(parameters.getOrganization(),
        OBFETConstants.SALES_ORDER_DOCBASETYPE, OBFETConstants.C_ORDER_TABLE));
    order.setAccountingDate(parameters.getAccountingDate());
    order.setOrderDate(parameters.getOrderDate());
    order.setWarehouse(parameters.getWarehouse());
    if (StringUtils.isNotEmpty(parameters.getInvoiceTerms())) {
      order.setInvoiceTerms(parameters.getInvoiceTerms());
    }
    order.setBusinessPartner(parameters.getBusinessPartner());
    order.setPartnerAddress(parameters.getBusinessPartnerLocation());
    order.setPriceList(parameters.getPriceList());
    order.setCurrency(parameters.getCurrency());
    order.setSummedLineAmount(BigDecimal.ZERO);
    order.setGrandTotalAmount(BigDecimal.ZERO);
    order.setSalesTransaction(parameters.isReceipt());
    order.setPaymentMethod(parameters.getPaymentMethod());
    order.setPaymentTerms(parameters.getPaymentTerms());

    OBDal.getInstance().save(order);
    OBDal.getInstance().flush();

    return order;
  }

  /**
   * Creates and saves a new {@link OrderLine} with the default parameters for the automated tests
   */
  public static OrderLine createNewSalesOrderLineWithDefaultParameters(Order order,
      Product product) {
    OBFETOrderLineParameters orderLineParameters = new OBFETOrderLineParameters(order, product);
    return OBFETUtility.createNewOrderLine(orderLineParameters);
  }

  /**
   * Creates and saves a new {@link OrderLine} with the default parameters for the automated tests
   */
  private static OrderLine createNewOrderLine(OBFETOrderLineParameters parameters) {
    // Create one line
    OrderLine orderLine = OBProvider.getInstance().get(OrderLine.class);
    orderLine.setOrganization(parameters.getOrganization());
    orderLine.setClient(parameters.getClient());
    orderLine.setSalesOrder(parameters.getOrder());
    orderLine.setOrderDate(parameters.getOrderDate());
    orderLine.setWarehouse(parameters.getWarehouse());
    orderLine.setCurrency(parameters.getCurrency());
    orderLine.setLineNo(parameters.getLineNo());
    orderLine.setProduct(parameters.getProduct());
    orderLine.setUOM(parameters.getUom());
    orderLine.setInvoicedQuantity(BigDecimal.ZERO);
    orderLine.setOrderedQuantity(parameters.getOrderedQuantity());
    orderLine.setUnitPrice(parameters.getNetUnitPrice());
    orderLine.setListPrice(parameters.getNetListPrice());
    orderLine.setPriceLimit(parameters.getPriceLimit());
    orderLine.setTax(parameters.getTaxRate());
    orderLine.setLineNetAmount(parameters.getLineNetAmount());

    OBDal.getInstance().save(orderLine);
    OBDal.getInstance().flush();

    return orderLine;

  }

  /**
   * Calls C_Order_Post method to process a {@link Order} given as a parameter
   */
  public static boolean processOrder(Order order) {
    return processDocument(order, OBFETConstants.C_ORDER_POST_ID);
  }

  private static boolean processDocument(BaseOBObject document, String processId) {
    OBContext.setAdminMode(true);
    try {
      Process process = null;
      process = OBDal.getInstance().get(Process.class, processId);
      final ProcessInstance pinstance = CallProcess.getInstance()
          .call(process, document.getId().toString(), null);
      return (pinstance.getResult() == 0L);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Calls C_Order_Post method to reactivate an already Processed {@link Order} given as a parameter
   */
  public static boolean reactivateOrder(Order order) {
    try {
      order.setDocumentStatus("CO");
      order.setDocumentAction("RE");
      OBDal.getInstance().flush();
      return processDocument(order, OBFETConstants.C_ORDER_POST_ID);
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  /**
   * Creates and saves a new {@link Invoice} with the default parameters for the automated tests
   */
  public static Invoice createNewSalesInvoiceWithDefaultParameters() {
    OBFETInvoiceHeaderParameters invoiceHeaderParameters = new OBFETInvoiceHeaderParameters();
    return OBFETUtility.createNewInvoice(invoiceHeaderParameters);
  }

  private static Invoice createNewInvoice(OBFETInvoiceHeaderParameters parameters) {

    // Create header
    Invoice invoice = OBProvider.getInstance().get(Invoice.class);
    invoice.setOrganization(parameters.getOrganization());
    invoice.setClient(parameters.getClient());
    invoice.setDocumentType(parameters.getDocumentType());
    invoice.setTransactionDocument(parameters.getDocumentType());
    invoice.setDocumentNo(FIN_Utility.getDocumentNo(parameters.getOrganization(),
        OBFETConstants.SALES_INVOICE_DOCBASETYPE, OBFETConstants.C_INVOICE_TABLE));
    invoice.setAccountingDate(parameters.getAccountingDate());
    invoice.setInvoiceDate(parameters.getInvoiceDate());
    invoice.setTaxDate(parameters.getTaxDate());
    invoice.setBusinessPartner(parameters.getBusinessPartner());
    invoice.setPartnerAddress(parameters.getBusinessPartnerLocation());
    invoice.setPriceList(parameters.getPriceList());
    invoice.setCurrency(parameters.getCurrency());
    invoice.setSummedLineAmount(BigDecimal.ZERO);
    invoice.setGrandTotalAmount(BigDecimal.ZERO);
    invoice.setWithholdingamount(BigDecimal.ZERO);
    invoice.setSalesTransaction(parameters.isReceipt());
    invoice.setPaymentMethod(parameters.getPaymentMethod());
    invoice.setPaymentTerms(parameters.getPaymentTerms());

    OBDal.getInstance().save(invoice);
    OBDal.getInstance().flush();

    return invoice;
  }

  /**
   * Creates and saves a new {@link InvoiceLine} with the default parameters for the automated tests
   */
  public static InvoiceLine createNewSalesInvoiceLineWithDefaultParameters(Invoice invoice,
      Product product) {
    OBFETInvoiceLineParameters invoiceLineParameters = new OBFETInvoiceLineParameters(invoice,
        product);
    return OBFETUtility.createNewInvoiceLine(invoiceLineParameters);

  }

  /**
   * Creates and saves a new {@link InvoiceLine} with the default parameters for the automated tests
   */
  private static InvoiceLine createNewInvoiceLine(OBFETInvoiceLineParameters parameters) {
    InvoiceLine invoiceLine = OBProvider.getInstance().get(InvoiceLine.class);
    invoiceLine.setOrganization(parameters.getOrganization());
    invoiceLine.setClient(parameters.getClient());
    invoiceLine.setInvoice(parameters.getInvoice());
    invoiceLine.setLineNo(parameters.getLineNo());
    invoiceLine.setProduct(parameters.getProduct());
    invoiceLine.setUOM(parameters.getUom());
    invoiceLine.setInvoicedQuantity(parameters.getInvoicedQuantity());
    invoiceLine.setUnitPrice(parameters.getNetUnitPrice());
    invoiceLine.setListPrice(parameters.getNetListPrice());
    invoiceLine.setPriceLimit(parameters.getPriceLimit());
    invoiceLine.setTax(parameters.getTaxRate());
    invoiceLine.setLineNetAmount(parameters.getLineNetAmount());

    OBDal.getInstance().save(invoiceLine);
    OBDal.getInstance().flush();

    return invoiceLine;
  }

  public static InvoiceLine reloadAndRefreshInvoiceLine(InvoiceLine invoiceLineParam) {
    // Reload and refresh InvoiceLine object to reflect latest changes in Database
    InvoiceLine invoiceLine = OBDal.getInstance().get(InvoiceLine.class, invoiceLineParam.getId());
    OBDal.getInstance().refresh(invoiceLine);
    return invoiceLine;
  }

  public static OrderLine reloadAndRefreshOrderLine(OrderLine orderLineParam) {
    // Reload and refresh OrderLine object to reflect latest changes in Database
    OrderLine orderLine = OBDal.getInstance().get(OrderLine.class, orderLineParam.getId());
    OBDal.getInstance().refresh(orderLine);
    return orderLine;
  }

  public static JSONArray createSelectedLinesFromOrder(Order order) {

    JSONArray selectedLines = new JSONArray();
    for (int i = 0; i < order.getOrderLineList().size(); i++) {
      try {
        OrderLine orderLine = order.getOrderLineList().get(i);
        JSONObject line = new JSONObject();
        line.put("uOM", orderLine.getUOM().getId());
        line.put("uOM$_identifier", orderLine.getUOM().getIdentifier());
        line.put("product", orderLine.getProduct().getId());
        line.put("product$_identifier", orderLine.getProduct().getIdentifier());
        line.put("lineNo", orderLine.getLineNo());
        line.put("orderedQuantity", orderLine.getOrderedQuantity().toString());
        line.put("operativeQuantity",
            orderLine.getOperativeQuantity() == null ? orderLine.getOrderedQuantity().toString()
                : orderLine.getOperativeQuantity().toString());
        line.put("id", orderLine.getId());
        line.put("salesOrder", order.getId());
        line.put("operativeUOM", orderLine.getOperativeUOM() == null ? orderLine.getUOM().getId()
            : orderLine.getOperativeUOM().getId());
        line.put("operativeUOM$_identifier",
            orderLine.getOperativeUOM() == null ? orderLine.getUOM().getIdentifier()
                : orderLine.getOperativeUOM().getIdentifier());
        line.put("orderQuantity", "");
        selectedLines.put(line);
      } catch (JSONException e) {
        log.error("Error in method createSelectedLinesFromOrder", e);
      }
    }
    return selectedLines;
  }
}
