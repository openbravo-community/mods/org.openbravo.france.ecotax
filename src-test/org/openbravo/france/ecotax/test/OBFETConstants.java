/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2020 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.france.ecotax.test;

public class OBFETConstants {

  private OBFETConstants() {
    throw new IllegalStateException("FETConstants class");
  }

  // F&B International Group Client
  static final String FNB_GROUP_CLIENT_ID = "23C59575B9CF467C9620760EB255B389";
  // F&B Espana Organization
  static final String FNB_ESPANA_ORG_ID = "B843C30461EA4501935CB1D125C9C25A";
  // Openbravo User ID
  static final String OPENBRAVO_USER_ID = "100";
  // F&B International Group Admin Role
  static final String FNB_INT_GROUP_ADMIN_ROLE_ID = "42D0EEB1C66F497A90DD526DC597E6F0";
  // Tax Category - IVA Normal
  static final String TAX_CATEGORY_ID = "E020A69A1E784DC39BE57C41D6D5DB4E";
  // Product Category - Alcholic
  static final String PRODUCT_CATEGORY_ID = "DC7F246D248B4C54BFC5744D5C27704F";
  // UOM - Unit
  static final String UOM_ID = "100";
  // Hoteles Buenas Noches, S.A. Business Partner
  static final String BUSINESS_PARTNER_ID = "9E6850C866BD4921AD0EB7F7796CE2C7";
  // Tarifa de ventas Price List
  static final String PRICELIST_ID = "AEE66281A08F42B6BC509B8A80A33C29";
  // Tarifa de ventas Price List Version
  static final String PRICELIST_VERSION_ID = "FDE536FE9D8C4B068C32CD6C3650B6B8";
  // 30 days Payment Term
  static final String PAYMENTTERM_ID = "66BA1164A7394344BB9CD1A6ECEED05D";
  // Ventas Externas Tax
  static final String TAX_ID = "4BF9470755AD4395AABCB77F5014CBE8";
  // AR Invoice Document Type
  static final String INVOICE_DOCTYPE_ID = "7FCD49652E104E6BB06C3A0D787412E3";
  // Standard Order Document Type
  static final String ORDER_DOCTYPE_ID = "466AF4B0136A4A3F9F84129711DA8BD3";
  // España Región Norte Warehouse
  static final String WAREHOUSE_ID = "B2D40D8A5D644DD89E329DC297309055";
  // Euro
  static final String EURO_ID = "102";

  // Invoice Terms Immediate
  static final String INVOICE_TERMS_IMMEDIATE = "I";
  // Sales Order Document Base Type
  static final String SALES_ORDER_DOCBASETYPE = "SOO";
  // Sales Invoice Document Base Type
  static final String SALES_INVOICE_DOCBASETYPE = "ARI";
  // c_Invoice Table
  static final String C_INVOICE_TABLE = "C_Invoice";
  // C_Order Table
  static final String C_ORDER_TABLE = "C_Order";
  // C_Order_Post process
  static final String C_ORDER_POST_ID = "104";
  // C_Invoice_Post process
  static final String C_INVOICE_POST_ID = "111";
  // Eco Tax DEA Suffix
  static final String ECO_TAX_DEA = "DEA_";
  // Eco Tax DEEE Suffix
  static final String ECO_TAX_DEEE = "DEEE_";

  // Eco Tax Type DEA
  static final String ECO_TAX_TYPE_DEA = "PHIECO_DEA";
  // Eco Tax Type DEEE
  static final String ECO_TAX_TYPE_DEEE = "PHIECO_DEEE";
}
