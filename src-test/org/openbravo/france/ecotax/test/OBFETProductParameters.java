/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2020 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.france.ecotax.test;

import java.math.BigDecimal;

import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.france.ecotax.PhiecoTaxcategory;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.common.uom.UOM;
import org.openbravo.model.financialmgmt.tax.TaxCategory;

public class OBFETProductParameters {

  private Client client;
  private Organization organization;
  private String name;
  private String searchKey;
  private ProductCategory productCategory;
  private TaxCategory taxCategory;
  private String productType;
  private UOM uom;
  private PhiecoTaxcategory phiecoDeee;
  private PhiecoTaxcategory phiecoDea;
  private BigDecimal phiecoDeeeQty;
  private BigDecimal phiecoDeaQty;
  private BigDecimal listPrice;
  private BigDecimal standardPrice;

  public OBFETProductParameters(String searchKey, String name, PhiecoTaxcategory phiecoDea,
      BigDecimal phiecoDeaQty, PhiecoTaxcategory phiecoDeee, BigDecimal phiecoDeeeQty,
      BigDecimal listPrice, BigDecimal standardPrice) {
    this.client = OBContext.getOBContext().getCurrentClient();
    this.organization = OBContext.getOBContext().getCurrentOrganization();
    this.name = name;
    this.searchKey = searchKey;
    this.productCategory = OBDal.getInstance()
        .get(ProductCategory.class, OBFETConstants.PRODUCT_CATEGORY_ID);
    this.taxCategory = OBDal.getInstance().get(TaxCategory.class, OBFETConstants.TAX_CATEGORY_ID);
    this.productType = "I";
    this.uom = OBDal.getInstance().get(UOM.class, OBFETConstants.UOM_ID);
    this.phiecoDea = phiecoDea;
    this.phiecoDeaQty = phiecoDeaQty;
    this.phiecoDeee = phiecoDeee;
    this.phiecoDeeeQty = phiecoDeeeQty;
    this.listPrice = listPrice;
    this.standardPrice = standardPrice;
  }

  public Client getClient() {
    return client;
  }

  public void setClient(Client client) {
    this.client = client;
  }

  public Organization getOrganization() {
    return organization;
  }

  public void setOrganization(Organization organization) {
    this.organization = organization;
  }

  public String getName() {
    return name;
  }

  public void setCommercialName(String name) {
    this.name = name;
  }

  public String getSearchKey() {
    return searchKey;
  }

  public void setSearchKey(String searchKey) {
    this.searchKey = searchKey;
  }

  public ProductCategory getProductCategory() {
    return productCategory;
  }

  public void setProductCategory(ProductCategory productCategory) {
    this.productCategory = productCategory;
  }

  public TaxCategory getTaxCategory() {
    return taxCategory;
  }

  public void setTaxCategory(TaxCategory taxCategory) {
    this.taxCategory = taxCategory;
  }

  public String getProductType() {
    return productType;
  }

  public void setProductType(String productType) {
    this.productType = productType;
  }

  public UOM getUom() {
    return uom;
  }

  public void setUom(UOM uom) {
    this.uom = uom;
  }

  public PhiecoTaxcategory getPhiecoDeee() {
    return phiecoDeee;
  }

  public void setPhiecoDeee(PhiecoTaxcategory phiecoDeee) {
    this.phiecoDeee = phiecoDeee;
  }

  public PhiecoTaxcategory getPhiecoDea() {
    return phiecoDea;
  }

  public void setPhiecoDea(PhiecoTaxcategory phiecoDea) {
    this.phiecoDea = phiecoDea;
  }

  public BigDecimal getPhiecoDeeeQty() {
    return phiecoDeeeQty;
  }

  public void setPhiecoDeeeQty(BigDecimal phiecoDeeeQty) {
    this.phiecoDeeeQty = phiecoDeeeQty;
  }

  public BigDecimal getPhiecoDeaQty() {
    return phiecoDeaQty;
  }

  public void setPhiecoDeaQty(BigDecimal phiecoDeaQty) {
    this.phiecoDeaQty = phiecoDeaQty;
  }

  public BigDecimal getListPrice() {
    return listPrice;
  }

  public void setListPrice(BigDecimal listPrice) {
    this.listPrice = listPrice;
  }

  public BigDecimal getStandardPrice() {
    return standardPrice;
  }

  public void setStandardPrice(BigDecimal standardPrice) {
    this.standardPrice = standardPrice;
  }
}
