/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2020 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.france.ecotax.test;

import java.math.BigDecimal;

import org.openbravo.dal.core.OBContext;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;

public class OBFETEcoTaxCategoryParameters {

  private Client client;
  private Organization organization;
  private String name;
  private String searchKey;
  private String phiecoType;
  private BigDecimal taxamt;

  public OBFETEcoTaxCategoryParameters(String searchKey, String name, String phiecoType,
      BigDecimal taxAmount) {
    this.client = OBContext.getOBContext().getCurrentClient();
    this.organization = OBContext.getOBContext().getCurrentOrganization();
    this.searchKey = searchKey;
    this.name = name;
    this.phiecoType = phiecoType;
    this.taxamt = taxAmount;
  }

  public Client getClient() {
    return client;
  }

  public void setClient(Client client) {
    this.client = client;
  }

  public Organization getOrganization() {
    return organization;
  }

  public void setOrganization(Organization organization) {
    this.organization = organization;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSearchKey() {
    return searchKey;
  }

  public void setSearchKey(String searchKey) {
    this.searchKey = searchKey;
  }

  public String getEcoTaxType() {
    return phiecoType;
  }

  public void setEcoTaxType(String ecoTaxType) {
    this.phiecoType = ecoTaxType;
  }

  public BigDecimal getTaxAmount() {
    return taxamt;
  }

  public void setTaxAmount(BigDecimal taxAmount) {
    this.taxamt = taxAmount;
  }

}
